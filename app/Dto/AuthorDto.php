<?php


namespace App\Dto;

use Carbon\Carbon;
use Spatie\DataTransferObject\DataTransferObject;

/**
 * Class AuthorDto
 * @package App\Dto
 */
class AuthorDto extends DataTransferObject
{
    public string $name;
    public Carbon $birthday;
    public ?string $bio;
}
