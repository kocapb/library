<?php


namespace App\Dto;

use Spatie\DataTransferObject\DataTransferObject;

/**
 * Class BookDto
 * @package App\Dto
 */
class BookDto extends DataTransferObject
{
    public ?string $name;
    public ?array $author_ids;
    public ?string $description;

}
