<?php


namespace App\Exceptions\Services\Book;

use Exception;

/**
 * Class CreateBookException
 * @package App\Exceptions\Services\Book
 */
class CreateBookException extends Exception
{
}
