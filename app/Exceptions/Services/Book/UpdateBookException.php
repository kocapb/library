<?php


namespace App\Exceptions\Services\Book;

use Exception;

/**
 * Class UpdateBookException
 * @package App\Exceptions\Services\Book
 */
class UpdateBookException extends Exception
{
}
