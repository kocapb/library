<?php

namespace App\Http\Controllers\Api\V1;

use App\Dto\BookDto;
use App\Http\Controllers\Controller;
use App\Http\Requests\Book\CreateRequest;
use App\Http\Requests\Book\UpdateRequest;
use App\Http\Resources\BookCollectionResource;
use App\Http\Resources\BookResource;
use App\Http\Resources\BookWithAuthorsResource;
use App\Models\Author;
use App\Services\BookService;
use Illuminate\Http\Request;
use App\Exceptions\Services\Book\UpdateBookException;
use Illuminate\Http\Resources\Json\JsonResource;
use Spatie\DataTransferObject\DataTransferObject;

/**
 * Class BookController
 * @package App\Http\ControllersApi\V1
 */
class BookController extends Controller
{
    private BookService $service;

    public function __construct(BookService $service)
    {
        $this->service = $service;
    }

    /**
     * Display a listing of the resource.
     *
     * @return BookCollectionResource
     */
    public function index(): BookCollectionResource
    {
        return new BookCollectionResource($this->service->getAllBooks());
    }

    /**
     * @param CreateRequest $request
     * @return BookResource
     */
    public function store(CreateRequest $request): BookResource
    {
        return new BookResource(
            $this->service->addBook(
                new BookDto($request->validated())
            )
        );
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return BookWithAuthorsResource
     */
    public function show(int $id): BookWithAuthorsResource
    {
        return new BookWithAuthorsResource($this->service->getBookWithAuthors($id));
    }

    /**
     * Display all book for author
     *
     * @param Author $author
     *
     * @return BookCollectionResource
     */
    public function allByAuthor(Author $author): BookCollectionResource
    {
        return new BookCollectionResource($this->service->getBooksByAuthorId($author->id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateRequest $request
     * @param int $id
     * @return BookWithAuthorsResource
     * @throws UpdateBookException
     */
    public function update(UpdateRequest $request, $id): BookWithAuthorsResource
    {
        return new BookWithAuthorsResource(
            $this->service->updateBook($id, new BookDto($request->validated()))
        );
    }
}
