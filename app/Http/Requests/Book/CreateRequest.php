<?php

namespace App\Http\Requests\Book;

use App\Http\Requests\BaseRequest;
use Illuminate\Validation\Rule;

/**
 * Class CreateRequest
 * @package App\Http\Requests\Book
 */
class CreateRequest extends BaseRequest
{
    public function rules(): array
    {
        return [
            'name' => 'string|required',
            'description' => 'string|nullable',
            'author_ids' => 'array|required',
            'author_ids.*' => [
                'int',
                Rule::exists('authors'),
                'required'
            ],
        ];
    }
}
