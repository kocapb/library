<?php


namespace App\Http\Requests\Book;

/**
 * Class UpdateRequest
 * @package App\Http\Requests\Book
 */
class UpdateRequest extends CreateRequest
{
    public function rules(): array
    {
        $data = parent::rules();

        $data['author_ids'] = 'array|nullable';

        return $data;
    }
}
