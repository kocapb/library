<?php


namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

/**
 * Class AuthorCollectionResource
 * @package App\Http\Resources
 */
class AuthorCollectionResource extends ResourceCollection
{
    public $collection = AuthorResource::class;
}
