<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

/**
 * Class BookCollectionResource
 * @package App\Http\Resources
 */
class BookCollectionResource extends ResourceCollection
{
    public $collection = BookResource::class;
}
