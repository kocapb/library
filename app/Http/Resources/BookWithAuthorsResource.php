<?php


namespace App\Http\Resources;

use App\Models\Book;
use Illuminate\Http\Request;

/**
 * Class BookWithAuthorsResource
 * @package App\Http\Resources
 * @mixin Book
 */
class BookWithAuthorsResource extends BookResource
{
    /**
     * @param Request $request
     * @return array
     */
    public function toArray($request): array
    {
        $data = parent::toArray($request);

        $data['authors'] = new AuthorCollectionResource($this->authors);

        return $data;
    }
}
