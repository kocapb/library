<?php

namespace App\Services;

use App\Dto\AuthorDto;
use App\Models\Author;
use Illuminate\Database\Eloquent\Model;

/**
 * Class AuthorService
 * @package App\Services
 */
class AuthorService
{
    /**
     * @param AuthorDto $dto
     * @return Model|Author
     */
    public function addAuthor(AuthorDto $dto): Author
    {
        return Author::query()->create([
            'name' => $dto->name,
            'birthday' => $dto->birthday->toDateTimeString(),
            'bio' => $dto->bio,
        ]);
    }
}
