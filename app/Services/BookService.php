<?php


namespace App\Services;

use App\Dto\BookDto;
use App\Exceptions\Services\Book\CreateBookException;
use App\Exceptions\Services\Book\UpdateBookException;
use App\Models\Book;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Throwable;

/**
 * Class BookService
 * @package App\Services
 */
class BookService
{
    /**
     * @return Collection|Book[]
     */
    public function getAllBooks(): Collection
    {
        return Book::all();
    }

    /**
     * @param BookDto $dto
     *
     * @return Book
     *
     * @throws CreateBookException
     */
    public function addBook(BookDto $dto): Book
    {
        DB::beginTransaction();

        try {
            $book = $this->createBook($dto);
            $book->authors()->sync($dto->author_ids);

            DB::commit();

            return $book;
        }
        catch (Throwable $exception) {
            DB::rollBack();
        }

        throw new CreateBookException($exception->getMessage());
    }

    public function updateBook(int $id, BookDto $dto): Book
    {
        DB::beginTransaction();

        try {
            /** @var Book $book */
            $book = Book::query()->findOrFail($id);

            $book->fill(Arr::except($dto->toArray(), ['author_ids']));
            $book->save();

            if ($dto->author_ids) {
                $book->authors()->sync($dto->author_ids);
            }

            DB::commit();

            return $book;
        }
        catch (Throwable $exception) {
            DB::rollBack();
        }

        throw new UpdateBookException($exception->getMessage());
    }

    /**
     * @param int $id
     * @return Book
     */
    public function getBookWithAuthors(int $id): Model
    {
        return Book::query()->where(['id' => $id])->with(['authors'])->first();
    }


    public function getBooksByAuthorId(string $authorId): Collection
    {
        return Book::query()
            ->whereHas('authors', function (Builder $builder) use ($authorId) {
                $builder->where('authors.id', $authorId);
            })->get();
    }

    /**
     * @return Model|Book
     */
    private function createBook(BookDto $dto): Model
    {
        return Book::query()->create([
            'name' => $dto->name,
            'description' => $dto->description
        ]);
    }
}
