<?php


namespace Database\Factories;

use App\Models\Author;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;
use Exception;

/**
 * Class AuthorFactory
 * @package Database\Factories
 */
class AuthorFactory extends Factory
{
    /**
     * @var string
     */
    protected $model = Author::class;

    public function definition(): array
    {
        return [
            'name' => $this->faker->name,
            'bio' => $this->faker->text,
            'birthday' => $this->generateRandomDate()
        ];
    }

    /**
     * @return string
     * @throws Exception
     */
    private function generateRandomDate(): string
    {
        $startDate = Carbon::now();
        $endDate = Carbon::now()->subYears($this->faker->randomDigit());

        return Carbon::createFromTimestamp(random_int($endDate->timestamp, $startDate->timestamp));
    }
}
