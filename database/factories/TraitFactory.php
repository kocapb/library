<?php


namespace Database\Factories;


use App\Models\Author;
use App\Models\Book;
use Illuminate\Database\Eloquent\Model;

/**
 * Trait TraitFactory
 * @package Database\Factories
 */
trait TraitFactory
{
    /**
     * @return Author
     */
    protected function createAuthor(): Model
    {
        return Author::factory()->create();
    }

    /**
     * @return Book
     */
    protected function createBook(): Model
    {
        return Book::factory()->create();
    }
}
