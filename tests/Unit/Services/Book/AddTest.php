<?php


namespace Tests\Unit\Services\Book;

use App\Dto\BookDto;
use App\Models\Author;
use App\Services\BookService;
use Database\Factories\TraitFactory;
use Faker\Factory;
use Tests\TestCase;

/**
 * Class AddTest
 * @package Services\Book
 */
class AddTest extends TestCase
{
    use TraitFactory;

    protected BookDto $dto;
    protected Author $author;
    protected BookService $service;

    protected function setUp(): void
    {
        parent::setUp();

        $faker = Factory::create();
        $this->service = new BookService();
        $this->author = $this->createAuthor();
        $this->dto = new BookDto([
            'name' => $faker->name,
            'description' => $faker->text,
            'author_ids' => [$this->author->getAttribute('id')],
        ]);
    }

    public function testSuccess(): void
    {
        $book = $this->service->addBook($this->dto);

        $this->assertDatabaseHas('books', [
            'id' => $book->getAttribute('id'),
            'name' => $this->dto->name,
            'description' => $this->dto->description
        ]);

        $this->assertDatabaseHas('author_book', [
            'book_id' => $book->getAttribute('id'),
            'author_id' => $this->author->getAttribute('id'),
        ]);

    }
}
