<?php


namespace Services\Book;


use App\Models\Author;
use App\Models\Book;
use App\Services\BookService;
use Database\Factories\TraitFactory;
use Tests\TestCase;

class GetAllBookByAuthor extends TestCase
{
    use TraitFactory;

    protected Book $bookOne;
    protected Book $bookTwo;
    protected Book $bookThree;
    protected Author $authorOne;
    protected Author $authorTwo;
    protected BookService $service;

    protected function setUp(): void
    {
        parent::setUp();

        $this->service = new BookService();
        $this->bookOne = $this->createBook();
        $this->bookTwo = $this->createBook();
        $this->bookThree = $this->createBook();
        $this->authorOne = $this->createAuthor();
        $this->authorTwo = $this->createAuthor();

        $this->bookOne->authors()->attach($this->authorOne->id);
        $this->bookTwo->authors()->attach($this->authorOne->id);
        $this->bookThree->authors()->attach($this->authorTwo->id);
    }

    public function testSuccess(): void
    {
        $result = $this->service->getBooksByAuthorId($this->authorOne->id);

        $this->assertEquals(
            $result->pluck('id')->toArray(),
            [$this->bookOne->id, $this->bookTwo->id]
        );
    }
}
