<?php


namespace Tests\Unit\Services\Book;

use App\Models\Author;
use App\Models\Book;
use App\Services\BookService;
use Database\Factories\TraitFactory;
use Tests\TestCase;

/**
 * Class GetWithAuthorTest
 * @package Services\Book
 */
class GetWithAuthorTest extends TestCase
{
    use TraitFactory;

    protected Book $book;
    protected Author $existAuthorOne;
    protected Author $existAuthorTwo;
    protected BookService $service;

    protected function setUp(): void
    {
        parent::setUp();

        $this->service = new BookService();
        $this->book = $this->createBook();
        $this->existAuthorOne = $this->createAuthor();
        $this->existAuthorTwo = $this->createAuthor();

        $this->book->authors()->sync([$this->existAuthorOne->id, $this->existAuthorTwo->id]);

    }

    public function testSuccess(): void
    {
        $result = $this->service->getBookWithAuthors($this->book->id);

        $this->assertEquals($result->id, $this->book->id);

        $this->assertEquals(
            $result->authors->pluck('id')->toArray(),
            [$this->existAuthorOne->id, $this->existAuthorTwo->id]
        );
    }
}
