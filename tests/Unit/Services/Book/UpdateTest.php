<?php


namespace Tests\Unit\Services\Book;

use App\Models\Book;
use Database\Factories\TraitFactory;

/**
 * Class UpdateTest
 * @package Services\Book
 */
class UpdateTest extends AddTest
{
    use TraitFactory;

    protected Book $book;

    protected function setUp(): void
    {
        parent::setUp();

        $this->book = $this->createBook();
    }

    public function testSuccess(): void
    {
        $this->service->updateBook($this->book->id, $this->dto);

        $this->assertDatabaseHas('books', [
            'name' => $this->dto->name,
            'description' => $this->dto->description
        ]);

        $this->assertDatabaseHas('author_book', [
            'author_id' => current($this->dto->author_ids),
            'book_id' => $this->book->id,
        ]);
    }
}
